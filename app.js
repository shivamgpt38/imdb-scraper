const cheerio = require('cheerio');
const request = require('request');
const readline = require('readline');
const chalk = require('chalk');

const rl = readline.createInterface({
    input:process.stdin,
    output:process.stdout
});

function getId(){
    rl.question(chalk.cyan('Enter page ID: '),(id) =>{
        let url = 'http://www.imdb.com/title/'+id;
        getData(url);
    })
}

function getData(url){
    request(url,(error,response,html)=>{
        if(error) return console.log(error);
        $ = cheerio.load(html);
        var name = '#title-overview-widget > div.vital > div.title_block > div > div.titleBar > div.title_wrapper > h1'; //Title
        var genres ='#titleStoryLine > div:nth-child(8) > a';
        var c ='';
        for(var i =0 ; i<genres.length ; i++){
           c += $('#titleStoryLine > div:nth-child(8) > a:nth-child('+i+')').text();
        }
        c = c.split(" ");
        c.shift(); //Genres
        var rating = '#title-overview-widget > div.vital > div.title_block > div > div.titleBar > div.title_wrapper > div > meta';
        var totalTime = '#title-overview-widget > div.vital > div.title_block > div > div.titleBar > div.title_wrapper > div > time';
        // to output time ==> console.log($(totalTime).text().trim());
        var releaseDate = '#titleDetails > div:nth-child(6)';
        console.log($(releaseDate).text().trim())
    })  

}

getId();